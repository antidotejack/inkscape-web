These instructions are subject to change, please email doctormo@gmail.com or join the `#team_website` channel on the [Inkscape RocketChat instance](https://chat.inkscape.org/) if
you are having issues getting a local instance working.

# Installing inkscape-web locally

## Get the code
Start by getting the source:

```sh
$ git clone git@gitlab.com:inkscape/inkscape-web.git
$ cd inkscape-web
```
## Set up the virtual environment
These instructions assume you have Python 3 and __pip__ (`>= v20.3`) installed. If you don't have pip installed (you probably do) you can install it with the instructions in the [pip docs](https://pip.pypa.io/en/stable/installing/).

```sh
$ python3 -m venv pythonenv                    # create a virtual env in the folder `pythonenv`
$ source pythonenv/bin/activate                # activate the virtual env
$ pip install --upgrade pip                    # securely upgrade pip
$ pip install -r requirements.txt              # install dependencies
```

## Bootstrap the website
```sh
$ ./utils/init
$ ./utils/manage makemigrations
$ ./utils/manage migrate
$ ./utils/manage runserver
```

Open the Django development server running on _http://localhost:8000/_ in your web browser (recommendation: Firefox or Chrome)

Log in with the following credentials:  
__username__ - `admin`  
__password__ - `123456`

## How to update 
### The Website Code:
```sh
$ git pull
$ ./utils/update
```

### The CMS Content:
```sh
$ ./utils/refresh-cms
```

You can visit the inkscape website wiki by clicking the following [link](https://wiki.inkscape.org/wiki/index.php/WebSite).
